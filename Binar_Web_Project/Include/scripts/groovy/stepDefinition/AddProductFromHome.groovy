package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class AddProductFromHome {

	@Then("User login to the website")
	public void user_login_to_the_website() {
		WebUI.callTestCase(findTestCase('Step Definition/Page_Login/Feature Login/LGI006 - User want to login using correct credential'),
				[:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click Jual button")
	public void user_click_Jual_button() {
		WebUI.callTestCase(findTestCase('Pages/Page_Home/Click Jual'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input Nama Produk")
	public void user_input_Nama_Produk() {
		WebUI.callTestCase(findTestCase('Pages/Pages_Add_Product/Input Nama Produk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input Harga Produk")
	public void user_input_Harga_Produk() {
		WebUI.callTestCase(findTestCase('Pages/Pages_Add_Product/Input Harga Produk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User choose the category")
	public void user_choose_the_category() {
		WebUI.callTestCase(findTestCase('Pages/Pages_Add_Product/Pilih Kategori'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input the description")
	public void user_input_the_description() {
		WebUI.callTestCase(findTestCase('Pages/Pages_Add_Product/Input Deskripsi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click Terbitkan button")
	public void user_click_Terbitkan_button() {
		WebUI.callTestCase(findTestCase('Pages/Pages_Add_Product/Click Terbitkan'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User delivered to login page")
	public void user_delivered_to_login_page() {
		WebUI.callTestCase(findTestCase('Pages/Page_Login/Verify Content'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}