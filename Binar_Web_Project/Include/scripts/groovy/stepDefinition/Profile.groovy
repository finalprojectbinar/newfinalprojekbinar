package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Profile {

	@Then("User login web using correct credential")
	public void user_login_web_using_correct_credential() {
		WebUI.callTestCase(findTestCase('Step Definition/Page_Login/Feature Login/LGI006 - User want to login using correct credential'),
			[('email_list') : 'kyoshitsugi@gmail.com', ('password_list') : 'admin123'], FailureHandling.STOP_ON_FAILURE)
		
	}
	
	@Then("User click user menu")
	public void user_click_user_menu() {
		sleep(2000)
		WebUI.callTestCase(findTestCase('Pages/Page_Home/Click User'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("User click dropdown option profile")
	public void user_click_dropdown_option_profile() {
		WebUI.callTestCase(findTestCase('Pages/Page_Home/Click Profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("User input nama profile {string}")
	public void user_input_nama_profile(String nama) {
		WebUI.callTestCase(findTestCase('Pages/Page_Profile/Input Nama'), [('Nama') : nama], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("User input dropdown kota {string}")
	public void user_input_dropdown_kota(String kota) {
		WebUI.callTestCase(findTestCase('Pages/Page_Profile/Dropdown Kota'), [('Kota') : kota], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("User input alamat {string}")
	public void user_input_alamat(String alamat) {
		WebUI.callTestCase(findTestCase('Pages/Page_Profile/Input Alamat'), [('Alamat') : alamat], FailureHandling.STOP_ON_FAILURE)
		
	}
	
	@Then("User input no hp {string}")
	public void user_input_no_hp(String no_hp) {
		WebUI.callTestCase(findTestCase('Pages/Page_Profile/Input No HP'), [('No Handphone') : no_hp], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("User click submit at profile page")
	public void user_click_submit_at_profile_page() {
		WebUI.callTestCase(findTestCase('Pages/Page_Profile/Click Submit'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("Success message {string}")
	public void success_message(String message) {
		WebUI.callTestCase(findTestCase('Pages/Page_Profile/Read Success Message'), [('exp_success_message') : message],
			FailureHandling.STOP_ON_FAILURE)
	}

}