package stepDefinition

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.Then


public class Login {
	@Then("User open browser")
	public void user_open_browser() {
		WebUI.openBrowser('https://secondhand-store.herokuapp.com/');
		WebUI.maximizeWindow();
	//	WebUI.navigateToUrl('https://secondhand-store.herokuapp.com/');
	}

	@Then("User close browser")
	public void user_close_browser() {
		WebUI.closeBrowser();
	}

	@Then("User click on Masuk button at Homepage")
	public void user_click_on_Masuk_button_at_Homepage() {

		WebUI.callTestCase(findTestCase('Pages/Page_Home/Click Masuk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click on Masuk button at Login page")
	public void user_click_on_Masuk_button_at_Login_page() {

		WebUI.callTestCase(findTestCase('Pages/Page_Login/Click Masuk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Error message will be shown {string} at Email field")
	public void error_message_will_be_shown_at_Email_field(String message) {

		WebUI.callTestCase(findTestCase('Pages/Page_Login/Read Required Field Email Message'), [('exp_message') : message], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input registered email only {string}")
	public void user_input_registered_email_only(String email) {

		WebUI.callTestCase(findTestCase('Pages/Page_Login/Input Email'), [('email') : email], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input registered password only {string}")
	public void user_input_registered_password_only(String password) {

		WebUI.callTestCase(findTestCase('Pages/Page_Login/Input Password'), [('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Error message will be shown {string} at Password field")
	public void error_message_will_be_shown_at_Password_field(String message) {

		WebUI.callTestCase(findTestCase('Pages/Page_Login/Read Required Field Password Message'), [('exp_message') : message],
		FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input email without format {string}")
	public void user_input_email_without_format(String email) {

		WebUI.callTestCase(findTestCase('Pages/Page_Login/Input Email'), [('email') : email], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input registered password {string}")
	public void user_input_registered_password(String password) {

		WebUI.callTestCase(findTestCase('Pages/Page_Login/Input Password'), [('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input email with incomplete format {string}")
	public void user_input_email_with_incomplete_format(String email) {

		WebUI.callTestCase(findTestCase('Pages/Page_Login/Input Email'), [('email') : email], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input registered email {string}")
	public void user_input_registered_email(String email) {

		WebUI.callTestCase(findTestCase('Pages/Page_Login/Input Email'), [('email') : email], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input valid email {string}")
	public void user_input_valid_email(String email) {

		WebUI.callTestCase(findTestCase('Pages/Page_Login/Input Email'), [('email') : email], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input invalid password {string}")
	public void user_input_invalid_password(String password) {

		WebUI.callTestCase(findTestCase('Pages/Page_Login/Input Password'), [('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Popup message will be shown {string}")
	public void popup_message_will_be_shown(String message) {

		WebUI.callTestCase(findTestCase('Pages/Page_Login/Read Popup Error Message'), [('exp_errMessage') : message], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input invalid email {string}")
	public void user_input_invalid_email(String email) {

		WebUI.callTestCase(findTestCase('Pages/Page_Login/Input Email'), [('email') : email], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input valid password {string}")
	public void user_input_valid_password(String password) {

		WebUI.callTestCase(findTestCase('Pages/Page_Login/Input Password'), [('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input unverified email {string}")
	public void user_input_unverified_email(String email) {

		WebUI.callTestCase(findTestCase('Pages/Page_Login/Input Email'), [('email') : email], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input unverified password {string}")
	public void user_input_unverified_password(String password) {

		WebUI.callTestCase(findTestCase('Pages/Page_Login/Input Password'), [('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click on link Daftar disini")
	public void user_click_on_link_Daftar_disini() {

		WebUI.callTestCase(findTestCase('Pages/Page_Login/Click Link Daftar'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
