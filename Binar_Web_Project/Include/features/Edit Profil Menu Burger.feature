@EditProfileMenuBurger
Feature: Add Product From Home
	As a user, I want to add a product in Secondhand Web.
	
	@DP001
	Scenario: EPMB001 - User want to edit profile with the valid data
		Then User login to the website
		Then User click menu burger
		Then User click edit on bar user
		Then User input nama
		Then user input kota
		Then user input alamat
		Then user input nomor handphone
		Then click submit
	
	@DP002
	Scenario: ADP002 - User does not input name
		Then User login to the website
		Then User click menu burger
		Then User click edit on bar user
		Then user input kota
		Then user input alamat
		Then user input nomor handphone
		Then click submit
	
		