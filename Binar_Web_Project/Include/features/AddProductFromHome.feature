@AddProductFromHome
Feature: Add Product From Home
	As a user, I want to add a product in Secondhand Web.
	
	@ADP003
	Scenario: ADP003 - User want to add a product from home page
		Then User login to the website
		Then User click Jual button
		Then User input Nama Produk
		Then User input Harga Produk
		Then User choose the category
		Then User input the description
		Then User click Terbitkan button
	
	@ADP004
	Scenario: ADP004 - User want to add a product without login
		Then User click Jual button
		Then User delivered to login page
	
		