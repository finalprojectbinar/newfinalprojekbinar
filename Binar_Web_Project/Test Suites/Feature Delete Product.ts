<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Feature Delete Product</description>
   <name>Feature Delete Product</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>3dd229ba-9860-4396-a51e-a7b4ce4a5772</testSuiteGuid>
   <testCaseLink>
      <guid>03ba7d26-8d2e-4a80-a49f-cee464008bc0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Daftar_Jual/Feature Delete Product/DP001 - User want to delete product</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1e3e4e77-c28d-43ae-8bf9-94a7472013cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step Definition/Page_Daftar_Jual/Feature Delete Product/DP002 - User want to delete the sold product</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
