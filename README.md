**PLATINUM CHALLANGE BINAR BOOTCAMP**

Project Repository ini digunakan untuk menyimpan hasil pengerjaan final project binar kelas Quality Assurance wave 4 untuk web dan mobile testing.
Test skenario untuk testing ini dapat di akses melalui:

- Web : https://docs.google.com/spreadsheets/d/1Xh8AbvuN5TupJyzE7x-0u9WCUBC-XAcUkvd26L4-Zas/edit#gid=1867286669
- Mobile : https://docs.google.com/spreadsheets/d/16-bC164fB1ziUxnxFvE8tnAKQKnploPhbAhMPJRdMDo/edit#gid=0

Kontributor pada repository ini diantaranya:

1. Jimmy
2. Leo
3. Rio
4. Syaira
5. Wisnu

Thanks to **Prima Elgania** as our Facilitator.
