package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.Then
import internal.GlobalVariable

public class Login {
	@Then("User launch the app")
	public void user_launch_the_app() {
		Mobile.startApplication('Apk/app-release.apk', false)
	}

	@Then("User close the app")
	public void user_close_the_app() {
		Mobile.closeApplication()
	}

	@Then("User tap on Akun menu")
	public void user_tap_on_Akun_menu() {
		Mobile.callTestCase(findTestCase('Pages/Page_Home/Tap Button Akun'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User tap on Masuk button at page Akun Saya")
	public void user_tap_on_Masuk_button_at_page_Akun_Saya() {
		Mobile.callTestCase(findTestCase('Pages/Page_Akun_Saya/Before Login/Tap Masuk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User tap on Masuk button at page Masuk")
	public void user_tap_on_Masuk_button_at_page_Masuk() {
		Mobile.callTestCase(findTestCase('Pages/Page_Login/Tap Button Masuk'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User will be shown error message {string} at Email field")
	public void user_will_be_shown_error_message_at_Email_field(String message) {
		Mobile.callTestCase(findTestCase('Pages/Page_Login/Read Error Message Email'), [('exp_error_message') : message],
		FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input registered email {string}")
	public void user_input_registered_email(String email) {
		Mobile.callTestCase(findTestCase('Pages/Page_Login/Input Email'), [('email') : email], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User will be shown error message {string} at Password field")
	public void user_will_be_shown_error_message_at_Password_field(String password) {
		Mobile.callTestCase(findTestCase('Pages/Page_Login/Read Error Message Password'), [('exp_error_message') : password],
		FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input registered password {string}")
	public void user_input_registered_password(String password) {
		Mobile.callTestCase(findTestCase('Pages/Page_Login/Input Password'), [('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input invalid email format {string}")
	public void user_input_invalid_email_format(String email) {
		Mobile.callTestCase(findTestCase('Pages/Page_Login/Input Email'), [('email') : email], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input invalid password {string}")
	public void user_input_invalid_password(String password) {
		Mobile.callTestCase(findTestCase('Pages/Page_Login/Input Password'), [('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User will be shown popup error message {string}")
	public void user_will_be_shown_popup_error_message(String string) {
	}

	@Then("User input invalid email {string}")
	public void user_input_invalid_email(String string) {
	}

	@Then("User tap on link Daftar")
	public void user_tap_on_link_Daftar() {
	}
}
