package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class BuyProduct {

	@Then("Buyer login to the app")
	public void buyer_login_to_the_app() {
		Mobile.callTestCase(findTestCase('Step Definition/Page_Login/Feature Login/LGI005 - User want to login using correct credential'), 
			[('email_list') : 'jimmytama96@gmail.com', ('password_list') : 'admin123'], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Buyer choose the product")
	public void buyer_choose_the_product() {
		Mobile.callTestCase(findTestCase('Pages/Page_Home/Tap Product Card'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Buyer click saya tertarik dan ingin nego button")
	public void buyer_click_saya_tertarik_dan_ingin_nego_button() {
		Mobile.callTestCase(findTestCase('Pages/Page_Product_Detail/Tap Saya tertarik dan Ingin Nego Button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Buyer input the prefered price")
	public void buyer_input_the_prefered_price() {
		Mobile.callTestCase(findTestCase('Pages/Page_Product_Detail/Insert Prefered Price'), [('harga') : '90000'], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Buyer click kirim button")
	public void buyer_click_kirim_button() {
		Mobile.callTestCase(findTestCase('Pages/Page_Product_Detail/Tap Kirim Button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Buyer delivered to login page")
	public void buyer_delivered_to_login_page() {
		Mobile.callTestCase(findTestCase('Pages/Page_Login/Verify Content'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}