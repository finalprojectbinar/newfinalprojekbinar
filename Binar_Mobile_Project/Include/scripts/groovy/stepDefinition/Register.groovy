package stepDefinition

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.Then

public class Register {
	@Then("User tap on Daftar link at page Masuk")
	public void user_tap_on_Daftar_link_at_page_Masuk() {
		Mobile.callTestCase(findTestCase('Pages/Page_Login/Tap Link Daftar'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User tap on Daftar button at Daftar page")
	public void user_tap_on_Daftar_button_at_Daftar_page() {
		Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Tap Button Daftar'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input email {string} at Daftar page")
	public void user_input_email_at_Daftar_page(String email) {
		Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Input Email'), [('email') : email], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input password {string} at Daftar page")
	public void user_input_password_at_Daftar_page(String password) {
		Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Input Password'), [('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input nomor hp {string} at Daftar page")
	public void user_input_nomor_hp_at_Daftar_page(String nomor_hp) {
		WebUI.callTestCase(findTestCase('Pages/Page_Daftar/Input Nomor HP'), [('nomor_hp') : nomor_hp], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input kota {string} at Daftar page")
	public void user_input_kota_at_Daftar_page(String kota) {
		Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Input Kota'), [('kota') : kota], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input alamat {string} at Daftar page")
	public void user_input_alamat_at_Daftar_page(String alamat) {
		Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Input Alamat'), [('alamat') : alamat], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User will be shown error message {string} at Nama field Daftar page")
	public void user_will_be_shown_error_message_at_Nama_field_Daftar_page(String message) {
		Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Read Error Message Nama'), [('exp_message') : message],
		FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input nama {string} at Daftar page")
	public void user_input_nama_at_Daftar_page(String nama) {
		Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Input Nama'), [('nama') : nama], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User will be shown error message {string} at Email field Daftar page")
	public void user_will_be_shown_error_message_at_Email_field_Daftar_page(String email) {
		Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Read Error Message Email'), [('exp_message') : email],
		FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User will be shown error message {string} at Password field Daftar page")
	public void user_will_be_shown_error_message_at_Password_field_Daftar_page(String password) {
		Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Read Error Message Password'), [('exp_message') : password],
		FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User will be shown error message {string} at Nomor HP field Daftar page")
	public void user_will_be_shown_error_message_at_Nomor_HP_field_Daftar_page(String nomor_hp) {
		Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Read Error Message Nomor HP'), [('exp_message') : nomor_hp],
		FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User will be shown error message {string} at Kota field Daftar page")
	public void user_will_be_shown_error_message_at_Kota_field_Daftar_page(String kota) {
		Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Read Error Message Kota'), [('exp_message') : kota],
		FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User will be shown error message {string} at Alamat field Daftar page")
	public void user_will_be_shown_error_message_at_Alamat_field_Daftar_page(String alamat) {
		Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Read Error Message Alamat'), [('exp_message') : alamat],
		FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User tap on Masuk link at page Daftar")
	public void user_tap_on_Masuk_link_at_page_Daftar() {
		WebUI.callTestCase(findTestCase('Pages/Page_Daftar/Tap Link Masuk'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("User Scroll page at Daftar page")
	public void user_Scroll_page_at_Daftar_page() {
		Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Scroll'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
