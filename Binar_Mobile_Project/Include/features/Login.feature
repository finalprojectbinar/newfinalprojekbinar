@Login
Feature: Login
As a user, I want to login in Secondhand Store App.

@LGI001
Scenario: LGI001 - User want to login without input email and password
Then User launch the app
Then User tap on Akun menu
Then User tap on Masuk button at page Akun Saya
Then User tap on Masuk button at page Masuk
Then User will be shown error message "Email tidak boleh kosong" at Email field
Then User close the app

@LGI002
Scenario: LGI002 - User want to login only input registered email
Then User launch the app
Then User tap on Akun menu
Then User tap on Masuk button at page Akun Saya
Then User input registered email "jimmytama96@gmail.com"
Then User tap on Masuk button at page Masuk
Then User will be shown error message "Password tidak boleh kosong" at Password field
Then User close the app

@LGI003
Scenario: LGI003 - User want to login only input registered password
Then User launch the app
Then User tap on Akun menu
Then User tap on Masuk button at page Akun Saya
Then User input registered password "admin123"
Then User tap on Masuk button at page Masuk
Then User will be shown error message "Email tidak boleh kosong" at Email field
Then User close the app

@LGI004
Scenario: LGI004 - User want to login without using format email
Then User launch the app
Then User tap on Akun menu
Then User tap on Masuk button at page Akun Saya
Then User input invalid email format "jimmytama"
Then User input registered password "admin123"
Then User tap on Masuk button at page Masuk
Then User will be shown error message "Email tidak valid" at Email field
Then User close the app

@LGI006
Scenario: LGI006 - User want to login using valid email and invalid password
Then User launch the app
Then User tap on Akun menu
Then User tap on Masuk button at page Akun Saya
Then User input registered email "jimmytama96@gmail.com"
Then User input invalid password "admin1"
Then User tap on Masuk button at page Masuk
Then User will be shown popup error message "Email atau kata sandi salah"
Then User close the app

@LGI007
Scenario: LGI007 - User want to login using invalid email and valid password
Then User launch the app
Then User tap on Akun menu
Then User tap on Masuk button at page Akun Saya
Then User input invalid email "jimmy@gmail.com"
Then User input registered password "admin123"
Then User tap on Masuk button at page Masuk
Then User will be shown popup error message "Email atau kata sandi salah"
Then User close the app

@LGI008
Scenario: LGI008 - User want to login using invalid email and invalid password
Then User launch the app
Then User tap on Akun menu
Then User tap on Masuk button at page Akun Saya
Then User input invalid email "jimmy@gmail.com"
Then User input invalid password "admin1"
Then User tap on Masuk button at page Masuk
Then User will be shown popup error message "Email atau kata sandi salah"
Then User close the app

@LGI009
Scenario: LGI009 - User want to access register page
Then User launch the app
Then User tap on Akun menu
Then User tap on Masuk button at page Akun Saya
Then User tap on link Daftar
Then User close the app

@LGI005
Scenario: LGI005 - User want to login using correct credential
Then User launch the app
Then User tap on Akun menu
Then User tap on Masuk button at page Akun Saya
Then User input registered email "jimmytama96@gmail.com"
Then User input registered password "admin123"
Then User tap on Masuk button at page Masuk
Then User close the app

