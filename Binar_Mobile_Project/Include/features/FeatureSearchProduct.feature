@SearchProduct
Feature: Search Product
  As a buyer, I want to search a product in secondhand app.

  @SPO001
  Scenario: SPO001 - User wants to search a product
    Then buyer tap the search button
    Then buyer input the name of the product on search bar
    Then the product appears

  @SPO002
  Scenario: SPO002 - User want to see all elektronik product
    Then buyer tap the elektronik button
    Then the product appears
    
  @SPO003
  Scenario: SPO003 - User want to see all Komputer dan Aksesoris product
    Then buyer tap the Komputer dan Aksesoris button
    Then the product appears
    
  @SPO004
  Scenario: SPO004 - User want to see Aksesoris Fashion product
    Then buyer tap the Aksesoris Fashion button
    Then the product appears
    
  @SPO005
  Scenario: SPO005 - User want to see all Buku dan Alat Tulis product
    Then buyer tap the Buku dan Alat Tulis button
    Then the product appears
    
  @SPO006
  Scenario: SPO006 - User want to see all Fashion Bayi dan Anak product
    Then buyer tap the Fashion Bayi dan Anak button
    Then the product appears