@AddProduct
Feature: Add Product from Home
	as a user, I want to add a product in Secondhand Mobile.
	
@ADP001
Scenario: ADP001 - User want to add product
	Then User login to the website
	Then User tap Tambah Produk
	Then User input Nama Produk
	Then User input Harga Produk
	Then User choose the category
	Then User input the description
	Then User input kota
	Then User click Terbitkan button
	
	@ADP002
	Scenario: ADP002 - User want to preview a product from Burger Menu
	Then User login to the website
	Then User click Burger Menu
	Then User click Tambah produk
	Then User input Nama Produk
	Then User input Harga Produk
	Then User choose the category
	Then User input kota
	Then User input the description
	Then User click Preview