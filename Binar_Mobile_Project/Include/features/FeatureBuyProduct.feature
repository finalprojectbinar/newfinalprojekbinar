@BuyProduct
Feature: Buy Product
	As a buyer, I want to buy a product in secondhand app.
	
	@BPO001
	Scenario: BPO001 - Buyer want to buy a product
		Then Buyer login to the app
		Then Buyer choose the product
		Then Buyer click saya tertarik dan ingin nego button
		Then Buyer input the prefered price
		Then Buyer click kirim button
	
	@BPO001
	Scenario: BPO002 - Buyer want to buy a product without login
		Then Buyer choose the product
		Then Buyer click saya tertarik dan ingin nego button
		Then Buyer delivered to login page