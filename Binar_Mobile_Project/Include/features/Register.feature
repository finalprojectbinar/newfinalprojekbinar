@Register
Feature: Register
As a user, I want to register in Secondhand Store App.

@REG001
Scenario: REG001 - User want to register without input data
Then User launch the app
Then User tap on Akun menu
Then User tap on Masuk button at page Akun Saya
Then User tap on Daftar link at page Masuk
Then User Scroll page at Daftar page
Then User tap on Daftar button at Daftar page
Then User close the app

@REG002
Scenario: REG002 - User want to register without input nama
Then User launch the app
Then User tap on Akun menu
Then User tap on Masuk button at page Akun Saya
Then User tap on Daftar link at page Masuk
Then User input email "testingemail@gmail.com" at Daftar page
Then User input password "xxxxxx" at Daftar page
Then User input nomor hp "000000000000" at Daftar page
Then User input kota "Jakarta" at Daftar page
Then User input alamat "Jl.cendrawasih" at Daftar page
Then User Scroll page at Daftar page
Then User tap on Daftar button at Daftar page
Then User will be shown error message "Nama tidak boleh kosong" at Nama field Daftar page
Then User close the app

@REG003
Scenario: REG003 - User want to register without input email
Then User launch the app
Then User tap on Akun menu
Then User tap on Masuk button at page Akun Saya
Then User tap on Daftar link at page Masuk
Then User input nama "Cahyadi" at Daftar page
Then User input password "xxxxxx" at Daftar page
Then User input nomor hp "000000000000" at Daftar page
Then User input kota "Jakarta" at Daftar page
Then User input alamat "Jl.cendrawasih" at Daftar page
Then User Scroll page at Daftar page
Then User tap on Daftar button at Daftar page
Then User will be shown error message "Email tidak boleh kosong" at Email field Daftar page
Then User close the app

@REG004
Scenario: REG004 - User want to register without input password
Then User launch the app
Then User tap on Akun menu
Then User tap on Masuk button at page Akun Saya
Then User tap on Daftar link at page Masuk
Then User input nama "Cahyadi" at Daftar page
Then User input email "testingemail@gmail.com" at Daftar page
Then User input nomor hp "000000000000" at Daftar page
Then User input kota "Jakarta" at Daftar page
Then User input alamat "Jl.cendrawasih" at Daftar page
Then User Scroll page at Daftar page
Then User tap on Daftar button at Daftar page 
Then User will be shown error message "Password tidak boleh kosong" at Password field Daftar page
Then User close the app

@REG005
Scenario: REG005 - User want to register without input nomor hp
Then User launch the app
Then User tap on Akun menu
Then User tap on Masuk button at page Akun Saya
Then User tap on Daftar link at page Masuk
Then User input nama "Cahyadi" at Daftar page
Then User input email "testingemail@gmail.com" at Daftar page
Then User input password "xxxxxx" at Daftar page
Then User input kota "Jakarta" at Daftar page
Then User input alamat "Jl.cendrawasih" at Daftar page
Then User Scroll page at Daftar page
Then User tap on Daftar button at Daftar page
Then User will be shown error message "Nomor telepon tidak boleh kosong" at Nomor HP field Daftar page
Then User close the app

@REG006
Scenario: REG006 - User want to register without input kota
Then User launch the app
Then User tap on Akun menu
Then User tap on Masuk button at page Akun Saya
Then User tap on Daftar link at page Masuk
Then User input nama "Cahyadi" at Daftar page
Then User input email "testingemail@gmail.com" at Daftar page
Then User input password "xxxxxx" at Daftar page
Then User input nomor hp "000000000000" at Daftar page
Then User input alamat "Jl.cendrawasih" at Daftar page
Then User Scroll page at Daftar page
Then User tap on Daftar button at Daftar page
Then User will be shown error message "Kota tidak boleh kosong" at Kota field Daftar page
Then User close the app

@REG007
Scenario: REG007 - User want to register without input alamat
Then User launch the app
Then User tap on Akun menu
Then User tap on Masuk button at page Akun Saya
Then User tap on Daftar link at page Masuk
Then User input nama "Cahyadi" at Daftar page
Then User input email "testingemail@gmail.com" at Daftar page
Then User input password "xxxxxx" at Daftar page
Then User input nomor hp "000000000000" at Daftar page
Then User input kota "Jakarta" at Daftar page
Then User Scroll page at Daftar page
Then User tap on Daftar button at Daftar page
Then User will be shown error message "Alamat tidak boleh kosong" at Alamat field Daftar page
Then User close the app

@REG009
Scenario: REG009 - User want to access login page
Then User launch the app
Then User tap on Akun menu
Then User tap on Masuk button at page Akun Saya
Then User tap on Daftar link at page Masuk
Then User Scroll page at Daftar page
Then User tap on Masuk link at page Daftar
Then User close the app

@REG008
Scenario: REG008 - User want to register using correct data
Then User launch the app
Then User tap on Akun menu
Then User tap on Masuk button at page Akun Saya
Then User tap on Daftar link at page Masuk
Then User input nama "Cahyadi" at Daftar page
Then User input email "testingemail1@gmail.com" at Daftar page
Then User input password "xxxxxx" at Daftar page
Then User input nomor hp "000000000000" at Daftar page
Then User input kota "Jakarta" at Daftar page
Then User input alamat "Jl.cendrawasih" at Daftar page
Then User Scroll page at Daftar page
Then User tap on Daftar button at Daftar page
Then User close the app




























