import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI




Mobile.callTestCase(findTestCase('Pages/Page_Home/Tap Button Akun'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Pages/Page_Akun_Saya/Before Login/Tap Masuk'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Pages/Page_Login/Tap Link Daftar'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Verify Content'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Input Nama'), [('nama') : 'Tania'], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Input Email'), [('email') : 'tania98@gmail.com'], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Input Password'), [('password') : 'xxxxxx'], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Input Nomor HP'), [('nomor_hp') : '00000000000'], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Input Kota'), [('kota') : 'Jakarta'], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Input Alamat'), [('alamat') : 'Jl.testing123'], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Scroll'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Pages/Page_Daftar/Tap Button Daftar'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Pages/Page_Akun_Saya/After Login/Verify Content'), [:], FailureHandling.STOP_ON_FAILURE)

