import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI




Mobile.callTestCase(findTestCase('Pages/Page_Home/Tap Button Akun'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Pages/Page_Akun_Saya/Before Login/Tap Masuk'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Pages/Page_Login/Verify Content'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Pages/Page_Login/Input Email'), [('email') : email_list], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Pages/Page_Login/Input Password'), [('password') : password_list], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Pages/Page_Login/Tap Button Masuk'), [:], FailureHandling.STOP_ON_FAILURE)

