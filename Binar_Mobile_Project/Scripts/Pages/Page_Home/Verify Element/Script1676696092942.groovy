import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_add_product'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Aksesoris_Fashion'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_akun'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_beranda'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Buku_dan_Alat_Tulis'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_elektronik'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Fashion_bayi_dan_Anak'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Fashion_Muslim'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Fotografi'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Handphone_dan_Aksesoris'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Hobi_dan_Koleksi'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Ibu_dan_Bayi'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Kesehatan'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_komputer_dan_aksesoris'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Makanan_dan_Minuman'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_notifikasi'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Olahraga_dan_Outdoor'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Otomotif'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Pakaian_Pria'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Pakaian_Wanita'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Perawatan_dan_Kecantikan'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Perlengkapan_Rumah'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_semua'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Sepatu_Pria'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Sepatu_Wanita'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Souvenir_dan_Pesta'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_tas_pria'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Tas_Wanita'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_transaksi'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/btn_Voucher'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/img_product_image'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/img_promo_banner'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/inputfield_search_bar'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/label_product_category'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/label_product_name'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/label_product_price'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/label_telusuri_kategori'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Home/product_card'), 0)

